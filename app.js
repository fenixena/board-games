const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const app = express();
const port = 3500;

const {getMainPage,
    getItemPage,
    getNewsPage,
getContactPage} = require('./routes/routes');

app.set('port', process.env.port || port);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/assets'));

const database = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'BoardGames',
    insecureAuth : true
});

database.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = database;

app.get('/', getMainPage);
app.get('/catalog/:itemname', getItemPage);
app.get('/games-news', getNewsPage);
app.get('/contact', getContactPage);


app.listen(port, () => {
    console.log(`Server started on ${port} port.`);
});