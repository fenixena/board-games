'use strict';

const SLIDER_QUERY = 'SELECT * FROM `slider` ORDER BY id ASC';

module.exports = {
    getMainPage: (req, res) => {
        const gamesList = "SELECT * FROM `games` ORDER BY id ASC";

        db.query(gamesList, (err, games) => {
            if (err) {
                res.redirect('/');
            }

            db.query(SLIDER_QUERY, (err, sliderItems) => {
                if (err) {
                    res.redirect('/');
                }

                res.render('index.ejs', {
                    title: "Настольные игры",
                    data: {
                        games: games,
                        sliderItems: sliderItems
                    }
                });
            })

        });
    },
    getItemPage: (req, res) => {
        console.log();
        db.query("SELECT * FROM `games` ORDER BY id ASC", (err, gameDetails) => {
            let gameObject;

            gameDetails.forEach((game) => {
                if (game.link === req.params.itemname) {
                    gameObject = game;
                }
            });

            if (err) {
                res.redirect('/404');
            }

            res.render('item.ejs', {
                title: "Детали игры",
                details: gameObject,
            })
        });
    },
    getNewsPage: (req, res) => {
        res.render('games-news.ejs', {
            title: "Новости настольных игр"
        });
    },
    getContactPage: (req, res) => {
        res.render('contact.ejs', {
            title: "Контакты"
        });
    }
};