'use strict';
(() => {
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            autoWidth: true,
            nav: true,
            dots: true,
            loop: true
        })
    });

    if ($('.news-wrapper').length) {
        getRSSNews();
    }

    if ($('.submit-button').length) {
        initFormSubmission();
    }
})();

function getRSSNews() {
    $.ajax('https://tesera.ru/news/rss/').then((data) => {
        $(data).find('item').each((index, item) => {
            appendItem($(item));
        });
    });
}

function appendItem($item) {
    const $newsItem = $(`<div class='news-item'>
                    <div class="news-item-title"></div>                                                               
                    <div class="news-item-description"></div>                                        
                </div>`);
    const itemMarkup = $($.parseHTML($item.find('description').html())[0].wholeText);
    $newsItem.find('.news-item-title').text($item.find('title').html());
    $newsItem.find('.news-item-description').html(itemMarkup);
    $('.news-wrapper').append($newsItem)
}

function initFormSubmission() {
    const name_field = $('.name');
    const email_field = $('.email');
    const message_field = $('.message');
    const name_label = $('.name-label');
    const message_label = $('.message-label');
    const email_label = $('.email-label');
    const email_regex = new RegExp('^\\S+@\\S+$');
    const form_header = $('.form-header');

    $('.submit-button').on('click', () => {
        name_label.toggleClass('hidden', !!name_field.val());
        email_label.toggleClass('hidden', !!email_field.val() && email_regex.test(email_field.val()));
        message_label.toggleClass('hidden', !!message_field.val());

        if (!$('label').is(':visible')) {
            name_field.val('');
            email_field.val('');
            message_field.val('');

            form_header.text('Ваше сообщение отправлено!');
            setTimeout(() => {
                form_header.text('Напишите нам');
            }, 2500);
        }
    })
}